<?php
/**
 * @file
 * Provides integration with AJAX Include Pattern for Modular Content.
 */

/**
 * Implements hook_library().
 */
function ajaxinclude_library() {
  $path = drupal_get_path('module', 'ajaxinclude');
  $ajaxinclude = libraries_get_path('ajaxinclude');
  $libraries['ajaxinclude'] = array(
    'title' => 'AJAX Include Pattern',
    'website' => 'http://drupal.org/project/ajaxinclude',
    'version' => '7.x-1.x',
    'js' => array(
      $ajaxinclude . '/dist/ajaxInclude.min.js' => array('weight' => -2),
      $ajaxinclude . '/dist/ajaxIncludePlugins.min.js' => array('weight' => -1),
      $path . '/js/ajaxinclude.load.min.js' => array('group' => JS_DEFAULT),
    ),
    'css' => array($path . '/css/ajaxinclude.css' => array()),
  );

  return $libraries;
}

/**
 * Implements hook_menu().
 */
function ajaxinclude_menu() {
  $items = array();
  $items['ajaxinclude'] = array(
    'page callback'    => 'ajaxinclude_response',
    'access arguments' => array('access content'),
  );
  $items['ajaxinclude/%/%'] = array(
    'page callback'    => 'ajaxinclude_block_response',
    'page arguments'   => array(1, 2),
    'access arguments' => array('access content'),
  );
  return $items;
}

/**
 * Displays the ajaxinclude blocks as aggregated contents.
 *
 * @todo better way for different cache, nocache, and path exclusion.
 * @todo consider cherry-picked locaStorage with limit 2.5-10MB?
 */
function ajaxinclude_response() {
  drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');

  // Prevents the ajaxinclude page itself from being cached if decided.
  ajaxinclude_revalidate();

  if (!is_null(arg(1))) {
    return;
  }

  if (!isset($_REQUEST["blocks"]) ){
    return MENU_NOT_FOUND;
  }

  $blocks = explode(",", $_REQUEST["blocks"]);
  $build = '';
  foreach ($blocks as $url) {
    list(, $prefix, $module, $delta) = explode('/', $url);
    $block = ajaxinclude_get_block($module, $delta);

    if (empty($block)) {
      continue;
    }
    $build .= '<entry url="' . $url . '">' . $block . "</entry>";
  }

  drupal_alter('ajaxinclude_response', $build, $blocks);
  print $build;
  drupal_exit();
}

/**
 * Disables cache for the ajaxinclude block output, or page itself.
 */
function ajaxinclude_revalidate() {
  if (variable_get('ajaxinclude_nocache', TRUE)) {
    // Disable browsers cache.
    drupal_add_http_header('Last-Modified', gmdate('D, d M Y H:i:s') . ' GMT');
    drupal_add_http_header('Cache-Control', 'no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0');
    drupal_add_http_header('Expires', 'Sat, 02 Jan 1971 00:00:00 GMT');
    drupal_add_http_header('Pragma', 'no-cache');

    // Disable server-side caching for this particular ajaxinclude.
    drupal_page_is_cacheable(FALSE);
  }
}

/**
 * Displays the ajaxinclude individual block content.
 */
function ajaxinclude_block_response($module, $delta) {
  if ($build = ajaxinclude_get_block($module, $delta)) {
    drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');
    print $build;
    // Do not drupal_exit() to make individual block cache, or nocache work.
    return NULL;
  }
  return MENU_NOT_FOUND;
}

/**
 * Checks access based on the given roles defined via UI.
 */
function _ajaxinclude_check_access($block, $settings = NULL) {
  global $user;

  // Hide non-ajaxinclude contents even from User 1.
  $access = empty($settings) ? FALSE : TRUE;

  // Always grant access for User 1 when it is an ajaxinclude.
  if ($user->uid == 1) {
    return TRUE;
  }

  // Only check access if roles are restricted.
  if (!empty($settings['module']) && $block->module == $settings['module'] && $block->delta == $settings['delta'] && !empty($settings['roles'])) {
    $rids    = array_keys($user->roles);
    $allowed = array_keys($settings['roles']);
    $check   = array_intersect($rids, $allowed);
    $access  = empty($check) ? FALSE : TRUE;
  }
  return $access;
}

/**
 * Retrieves the ajaxinclude block content.
 */
function ajaxinclude_get_block($module, $delta = NULL, $wrapper = FALSE) {
  $build = array();
  $block = block_load($module, $delta);
  if (!isset($block->bid)) {
    return;
  }

  $settings = ajaxinclude_get_settings($block);
  if (empty($settings)) {
    return;
  }

  if (! _ajaxinclude_check_access($block, $settings)) {
    return;
  }

  // Block is cached by default, yet can be disabled if so configured.
  if (!empty($settings['nocache'])) {
    ajaxinclude_revalidate();
  }

  if ($wrapper) {
    $block_content = _block_render_blocks(array($block));
    $build = _block_get_renderable_array($block_content);
  }
  else {
    $block_content = module_invoke($module, 'block_view', $delta);
    $build = empty($block_content['content']) ? array() : $block_content['content'];
  }

  if ($build) {
    $build = is_string($build) ? array('#type' => 'markup', '#markup' => $build) : $build;

    $attributes = array('class' => array('ajaxinclude__content'));
    $attributes['id'] = drupal_clean_css_identifier('ajaxinclude-content-' . $module . '-' . $delta);
    $build['#prefix'] = '<div' . drupal_attributes($attributes) . '>';
    $build['#suffix'] = '</div>';

    drupal_alter('ajaxinclude_block', $build, $block, $settings);
    return drupal_render($build);
  }
  return FALSE;
}

/**
 * Stores ajaxinclude URLs temporarily.
 */
function ajaxinclude_list($url = NULL, $settings = array()) {
  static $list = array();
  if (!is_null($url) && empty($settings['noconcat'])) {
    $list[$url] = $settings;
  }
  return $list;
}

/**
 * Implements hook_preprocess_html().
 */
function ajaxinclude_preprocess_html(&$variables) {
  $list = ajaxinclude_list();
  $js['ajaxinclude']['list'] = count($list) < 2 ? FALSE: TRUE;
  drupal_add_js($js, 'setting');
}

/**
 * Implement hook_processes_block().
 */
function ajaxinclude_preprocess_block(&$variables) {
  $path     = drupal_get_path('module', 'ajaxinclude');
  $block    = $variables['block'];
  $settings = ajaxinclude_get_settings($block);

  if (empty($settings)) {
    return;
  }

  $loader = array(
    '#settings' => $settings,
    '#attached' => array(
      'library' => array(array('ajaxinclude', 'ajaxinclude')),
    ),
    '#attributes' => array(
      'class' => array('ajaxinclude-loader'),
    ),
  );

  if ($method = $settings['method']) {
    $url      = base_path() . 'ajaxinclude/' . $block->module . '/' . $block->delta;
    $element  = $settings['element'];
    $selector = $element == 'block' ? '' : $element . '_';

    $variables['classes_array'][] = 'ajaxinclude';
    $variables[$selector . 'attributes_array']['data-' . $method] = $url;
    $variables[$selector . 'attributes_array']['data-ajaxinclude'] = "";
    $variables[$selector . 'attributes_array']['data-include'] = "true";

    if ($target = $settings['target']) {
      $variables[$selector . 'attributes_array']['data-target'] = $target;
    }

    if (!empty($settings['noconcat'])) {
      $url = '';
      $variables[$selector . 'attributes_array']['data-exclude'] = "true";
      unset($variables[$selector . 'attributes_array']['data-include']);
    }

    if (!empty($settings['interaction'])) {
      $url = '';
      $variables['classes_array'][] = 'ajaxinclude--interaction';
      $variables[$selector . 'attributes_array']['data-interaction'] = $settings['interaction'];
      unset($variables[$selector . 'attributes_array']['data-include']);
    }

    if (!empty($settings['minheight'])) {
      $variables['attributes_array']['data-min-height'] = (int) $settings['minheight'];
    }

    if (!empty($settings['class'])) {
      $variables['classes_array'][] = strip_tags($settings['class']);
    }

    if ($media = $settings['media']) {
      $variables[$selector . 'attributes_array']['data-media'] = $media;
      $match = libraries_get_path('matchmedia');
      $options = array('group' => JS_LIBRARY, 'weight' => -4);
      $loader['#attached']['js'][$match  . '/matchMedia.js'] = $options;
      $loader['#attached']['js'][$match  . '/matchMedia.addListener.js'] = $options;
    }

    if ($preloader = $settings['preloader']) {
      list($skin, $count) = array_pad(array_map('trim', explode(":", $preloader, 2)), 2, NULL);
      $loader['#attached']['css'][] = $path . '/css/spinkit/ajaxinclude.' . $skin . '.css';
      $loader['#attributes']['class'][] = 'ajaxinclude-loader--' . $skin;
      $loader['#attributes']['data-spin'] = (int) $count;
    }

    if (!empty($settings['noscript'])) {
      $loader['#suffix'] = '<noscript class="ajaxinclude__noscript">' . $variables['content'] . '</noscript>';
    }

    if (!empty($url)) {
      ajaxinclude_list($url, $settings);
    }

    // Sanitized values by drupal_attributes here and elsewhere.
    $loader['#markup'] = '<div' . drupal_attributes($loader['#attributes']) . '></div>';
    $variables['content'] = drupal_render($loader);
  }
}

/**
 * Gets ajaxinclude settings for the given block.
 */
function ajaxinclude_get_settings($block) {
  $settings = empty($block->ajaxinclude) ? NULL : unserialize($block->ajaxinclude);
  // @fixme re-stringified, and loaded twice after AJAX.
  if ($settings && is_string($settings)) {
    $settings = unserialize($settings);
  }
  return $settings;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function ajaxinclude_form_alter(&$form, &$form_state, $form_id) {
  if (user_access('administer blocks') && ($form_id == 'block_admin_configure' || $form_id == 'block_add_block_form')) {
    form_load_include($form_state, 'inc', 'ajaxinclude', 'includes/ajaxinclude.admin');
    _ajaxinclude_form_alter($form, $form_state, $form_id);
  }
}

/**
 * Implements hook_help().
 */
function ajaxinclude_help($path, $arg) {
  switch ($path) {
    case 'admin/help#ajaxinclude':
      return check_markup(file_get_contents(dirname(__FILE__) . "/README.txt"));
  }
}
